﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaClase
{
    class Program
    {
        public  class Deposito
        {
            public int Id { get; set; }
            public int Id_Usuario { get; set; }
            public decimal Cantidad { get; set; }

            public Deposito(int IdUsuario, decimal cantidad) 
            {
                this.Id += 1;
                this.Id_Usuario = IdUsuario;
                this.Cantidad = cantidad;
            }
        }
        public  class Retiro
        {
            public int Id { get; set; }
            public int Id_Usuario { get; set; }
            public decimal Cantidad { get; set; }

            public Retiro(int IdUsuario, decimal cantidad)
            {
                this.Id += 1;
                this.Id_Usuario = IdUsuario;
                this.Cantidad = cantidad;
            }
        }
        public class Usuarios
        {
            public int Id { get; set; }
            public string Nombre { get; set; }
            public decimal Dinero { get; set; }

            public List<Deposito> Depositos = new List<Deposito>();
            public List<Retiro> Retiros = new List<Retiro>();
            public Usuarios(string Nombre) 
            {
                this.Id += 1;
                this.Nombre = Nombre;
                this.Dinero = 1000;
            }

            public void Deposito(decimal deposito) 
            {
                Depositos.Add(new Deposito(this.Id,deposito));
                this.Dinero += deposito;

            }
            public void Retiro(decimal retiro)
            {
                if (this.Dinero != 0)
                {
                    Retiros.Add(new Retiro(this.Id, retiro));

                    if (retiro > this.Dinero)
                    {
                        this.Dinero = 0;
                    }
                    else
                    {
                        this.Dinero -= retiro;
                    }
                }
            }
        }

        public class Bank 
        {
            public int Id { get; set; }
            public string Nombre { get; set; }
            public decimal CantidadTotal { get; set; }

            public List<Usuarios> Usuarios = new List<Usuarios>();

            public Bank(string Nombre) 
            {
                this.Id += 1;
                this.Nombre = Nombre;
                this.CantidadTotal = 0;
            }
            public void AfiliarUsuario(Usuarios nuevoUsuario) 
            {
                this.Usuarios.Add(nuevoUsuario);
            }

            public decimal CalcularCantidadDeDineroTotal()
            {
                foreach (Usuarios x in Usuarios) 
                {
                    this.CantidadTotal += x.Dinero;
                }

                return this.CantidadTotal;
            }
        }

        static void Main(string[] args)
        {
            Usuarios persona1 = new Usuarios("Luis Coronado");
            Usuarios persona2 = new Usuarios("Miguel Moreta");
            Usuarios persona3 = new Usuarios("Juana Margarita");

            Bank nuevoBanco = new Bank("ElNuevoBanco");

            // Agregamos las personas al banco 
            nuevoBanco.AfiliarUsuario(persona1);
            nuevoBanco.AfiliarUsuario(persona2);
            nuevoBanco.AfiliarUsuario(persona3);

            // persona 1
            persona1.Deposito(1222);
            persona1.Deposito(1000);
            persona1.Deposito(1000);

            // Persona 2
            persona2.Deposito(10000);

            // Persona 3
            persona3.Deposito(320);
            persona3.Deposito(458);
            persona3.Deposito(500);
            persona3.Deposito(690);

            // Retiros
            persona1.Retiro(250);
            persona2.Retiro(500);
            persona3.Retiro(1000);

            // 
            nuevoBanco.CalcularCantidadDeDineroTotal();

            foreach (Usuarios persona in nuevoBanco.Usuarios)
            {
                Console.WriteLine("{0} tiene {1:C}",persona.Nombre, persona.Dinero);
                Console.WriteLine("Depositos {0} | Retiros {1}", persona.Depositos.Count, persona.Retiros.Count);
                Console.WriteLine("");
            }

            Console.WriteLine("{0} posee {1:C}", nuevoBanco.Nombre,nuevoBanco.CantidadTotal);

            Console.ReadLine();
        }
    }
}
